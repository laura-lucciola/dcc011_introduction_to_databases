Updated at 12/10/2017

#question_number|#lines|#columns|#SQL query
containing the following lines:
1 | 10 | 6 | SELECT * FROM movie LIMIT 10;
2 | 282 | 4 | SELECT * FROM scroll roll WHERE name = 'manager';

## Scheme 3 Relational

The database to be created and consulted has the following relational schema:

movie(movie id, movie name, production year, votes, ranking, rating)

movie info(movie id, movie genre id, note)
movie info[movie id] REFERENCIA movie[movie id]
movie info[movie genre id] REFERENCIA movie genre[movie genre id]

movie_genre(movie_genre_id, genre name)

person(person id, person name, gender)

role(person id, movie id, role name, role type id)
	role[person id] REFERENCIA person[person id]
	role[movie id] REFERENCIA movie[movie id]
	role[role type id] REFERENCIA role type[role type id]

role_type(role_type_id, type_name)

#### 4.1 SQL Commands

1.List the names of all movies directed by women and the name of their director.

2.List the 10 actors (or actresses) with the highest number of roles in films of the genre Crime
and a total amount of roles on these movies.

5.List the name of the movies in which Quentin Tarantino acted (remember that are movies in which
he acted and was director, films in which he is only director and films in which he
just acted) and the name of the role in each of them.

6.List the name, year of production and ranking position of the 20 best placed films
In the top 250 produced after the year 2000. Order by the best ranked.

7.List all the role types and a number movies associated to them. Order by the
amount, in a decreasing manner.

8.List of the names of movie genres and average votes (rating) of their
movies, ordered by the films that have the highest average first.


//----------------------------------------------------------------------------------------------------
3.List of the names of the directors of the ten films with the highest number of votes received
by the IMDB community in the genre Thriller, the names of their respective films and the
amount of votes received for each movie.

4.List a number of movies of each genre. Order by quantity of growing.

9.List a global percentage of actors of each sex considering all holdings
(paper) ordered by this percentage in a decreasing way.

10.About the trilogy of the film Lord of the Rings, a list of trilogy films,
Name of the people who work on the films, as well as the name of the
One played and the kind of paper. Order by year of production in ascending order.

11.List of names of the 10 actors or actresses who participate most in different genres
of movies. Also list a number of different genres that each participant and
order decreasingly.
