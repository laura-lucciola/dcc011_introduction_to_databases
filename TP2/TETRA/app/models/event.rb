class Event < ActiveRecord::Base
  self.table_name = 'Event'
  self.primary_key = :event_id

  has_many :talks, class_name: 'Talk', foreign_key: :event_id
end
