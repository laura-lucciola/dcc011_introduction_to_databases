class Talk < ActiveRecord::Base
  self.table_name = 'Talk'
  self.primary_key = :talk_id

  has_many :related_talks, class_name: 'RelatedTalk'
  has_many :related_talks, class_name: 'RelatedTalk'
  belongs_to :event, class_name: 'Event', foreign_key: :event_id
  belongs_to :speaker, class_name: 'Speaker', foreign_key: :speaker_id
  has_many :talk_ratings, class_name: 'TalkRating', foreign_key: :talk_id
  has_many :talk_tags, class_name: 'TalkTag', foreign_key: :talk_id
end
