class Rating < ActiveRecord::Base
  self.table_name = 'Rating'
  self.primary_key = :rating_id

  has_many :talk_ratings, class_name: 'TalkRating', foreign_key: :rating_id
end
