class TalkTag < ActiveRecord::Base
  self.table_name = 'Talk_Tag'
  self.primary_key = %i[talk_id tag_id]

  belongs_to :tag, class_name: 'Tag', foreign_key: :tag_id
  belongs_to :talk, class_name: 'Talk', foreign_key: :talk_id
end
