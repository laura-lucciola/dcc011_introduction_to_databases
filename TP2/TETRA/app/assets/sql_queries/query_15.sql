SELECT title,
CONCAT(
  REVERSE(
    SUBSTRING_INDEX(
      REVERSE(
        SUBSTR(transcript, 1, LOCATE("Brazil", transcript)-1)), ".", 1
	  )
	), 
	SUBSTRING_INDEX(
	  SUBSTR(transcript, LOCATE("Brazil", transcript)), ".", 1
	)
) as snippet
FROM Talk WHERE transcript LIKE "%Brazil%";
