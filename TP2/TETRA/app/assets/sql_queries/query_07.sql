SELECT event_name, COUNT(DISTINCT tag_name) as num_distinct_tags
FROM Event NATURAL JOIN Talk NATURAL JOIN Talk_Tag NATURAL JOIN Tag
GROUP BY event_id
ORDER BY num_distinct_tags DESC
LIMIT 10;
