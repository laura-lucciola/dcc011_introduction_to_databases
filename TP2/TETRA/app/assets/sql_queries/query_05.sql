SELECT Rating.rating_name, Talk.title, Talk_Rating.count
FROM Talk NATURAL JOIN Talk_Rating NATURAL JOIN Rating
GROUP BY Rating.rating_id, Talk.talk_id, Talk_Rating.talk_id
ORDER BY Talk_Rating.count DESC
LIMIT 15;

