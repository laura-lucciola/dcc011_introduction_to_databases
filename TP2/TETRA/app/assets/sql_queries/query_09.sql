SELECT title, rating_name, `count`
FROM Talk NATURAL JOIN Talk_Rating NATURAL JOIN Rating NATURAL JOIN
(
    SELECT rating_id, Max(`count`) AS max_count
    FROM   Talk_Rating
    GROUP  BY rating_id
) AS rating_max
WHERE  rating_max.max_count = `count`;
