SELECT S.speaker_name, COUNT(DISTINCT T.talk_id) AS num_talks, 
COUNT(DISTINCT E.country) as num_countries, SUM(T.views) total_views, 
COUNT(DISTINCT TT.tag_id) as num_distinct_tags, 
AVG(TR.count) as avg_ratings, MAX(E.year) as last_year 
FROM Speaker S NATURAL JOIN Talk T NATURAL JOIN Event as E 
NATURAL JOIN Talk_Tag as TT NATURAL JOIN Talk_Rating as TR 
GROUP BY S.speaker_id
ORDER BY num_talks DESC;