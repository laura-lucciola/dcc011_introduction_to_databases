SELECT country, MAX(count) as max_rating_count
FROM Event NATURAL JOIN Talk NATURAL JOIN Talk_Rating NATURAL JOIN
(
    SELECT rating_id, rating_name
    FROM Rating
    ORDER BY global_average DESC
    LIMIT 10
) as top_positive_ratings
WHERE country IS NOT NULL
GROUP BY country
ORDER BY max_rating_count DESC;
