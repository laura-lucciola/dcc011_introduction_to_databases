DROP DATABASE IF EXISTS ted_talks;
CREATE DATABASE ted_talks;
USE ted_talks;

# Tables
CREATE TABLE Tag(
    tag_id INT(6) PRIMARY KEY,
    tag_name VARCHAR(100) NOT NULL
) ENGINE=INNODB;

CREATE TABLE Speaker(
    speaker_id INT(6) PRIMARY KEY,
    speaker_name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL,
    occupation VARCHAR(100) CHARACTER SET utf8mb4
) ENGINE=INNODB;

CREATE TABLE Rating(
    rating_id INT(6) PRIMARY KEY,
    rating_name VARCHAR(100) NOT NULL,
    global_average FLOAT(10, 2)
) ENGINE=INNODB;

CREATE TABLE Event(
    event_id INT(6) PRIMARY KEY,
    event_name VARCHAR(100) NOT NULL,
    country VARCHAR(100),
    year INT(6)
) ENGINE=INNODB;

CREATE TABLE Talk(
    talk_id INT(6) PRIMARY KEY,
    title VARCHAR(100) NOT NULL,
    event_id INT(6) NOT NULL,
    speaker_id INT(6) NOT NULL,
    description TEXT CHARACTER SET utf8mb4,
    views INT(10),
    comments INT(10),
    published_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    transcript TEXT CHARACTER SET utf8mb4
) ENGINE=INNODB;

CREATE TABLE Talk_Rating(
    talk_id INT(6),
    rating_id INT(6),
    count INT(6) NOT NULL,
    PRIMARY KEY(talk_id, rating_id)
)  ENGINE=INNODB;

CREATE TABLE Talk_Tag(
    talk_id INT(6),
    tag_id INT(6),
    PRIMARY KEY(talk_id, tag_id)
)  ENGINE=INNODB;

CREATE TABLE Related_Talks(
    talk1_id INT(6),
    talk2_id INT(6),
    PRIMARY KEY(talk1_id, talk2_id)
)  ENGINE=INNODB;

# Foreign keys
ALTER TABLE Talk ADD CONSTRAINT fk_Talk_event_id FOREIGN KEY (event_id) REFERENCES Event(event_id);
ALTER TABLE Talk ADD CONSTRAINT fk_Talk_speaker_id FOREIGN KEY (speaker_id) REFERENCES Speaker(speaker_id);

ALTER TABLE Talk_Rating ADD CONSTRAINT fk_Talk_Rating_talk_id FOREIGN KEY (talk_id) REFERENCES Talk(talk_id);
ALTER TABLE Talk_Rating ADD CONSTRAINT fk_Talk_Rating_rating_id FOREIGN KEY (rating_id) REFERENCES Rating(rating_id);

ALTER TABLE Talk_Tag ADD CONSTRAINT fk_Talk_Tag_talk_id FOREIGN KEY (talk_id) REFERENCES Talk(talk_id);
ALTER TABLE Talk_Tag ADD CONSTRAINT fk_Talk_Tag_tag_id FOREIGN KEY (tag_id) REFERENCES Tag(tag_id);

ALTER TABLE Related_Talks ADD CONSTRAINT fk_Related_Talks_talk1_id FOREIGN KEY (talk1_id) REFERENCES Talk(talk_id);
ALTER TABLE Related_Talks ADD CONSTRAINT fk_Related_Talks_talk2_id FOREIGN KEY (talk2_id) REFERENCES Talk(talk_id);